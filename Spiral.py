#Author-Dag Rende
#Description-Archimedes spiral path in  a sketch

import adsk.core, adsk.fusion, adsk.cam, traceback, math

def run(context):
    ui = None
    try:
        innerRadius = 1.0
        outerRadius = 4.0
        pitch = 1.0

        app = adsk.core.Application.get()
        design = app.activeProduct
        timeLine = design.timeline
        timeLineGroups = timeLine.timelineGroups
        ui  = app.userInterface
        component = design.rootComponent
        sketches = component.sketches
        sketch = sketches.add(component.xYConstructionPlane)
        sketch.name = "spiral"

        totRevs = (outerRadius - innerRadius) / pitch
        revSteps = 16 
        vStep = 2.0 * math.pi / revSteps
        totV = totRevs * 2 * math.pi

        points = adsk.core.ObjectCollection.create()

        def addPoint(r, v):
            p1 = adsk.core.Point3D.create(r * math.cos(v), r * math.sin(v))
            points.add(p1)

        v = 0
        b = pitch / (2 * math.pi)
        while v < totV:
            r = innerRadius + b * v
            addPoint(r, v)
            v += vStep
        # add last point
        addPoint(outerRadius, totV)

        sketch.sketchCurves.sketchFittedSplines.add(points) 

    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))
